const books = [
  { 
    author: "Скотт Бэккер",
    name: "Тьма, что приходит прежде",
    price: 70 
  }, 
  {
   author: "Скотт Бэккер",
   name: "Воин-пророк",
  }, 
  { 
    name: "Тысячекратная мысль",
    price: 70
  }, 
  { 
    author: "Скотт Бэккер",
    name: "Нечестивый Консульт",
    price: 70
  }, 
  {
   author: "Дарья Донцова",
   name: "Детектив на диете",
   price: 40
  },
  {
   author: "Дарья Донцова",
   name: "Дед Снегур и Морозочка",
  }
];


const root = document.getElementById("root");
const ul = document.createElement("ul");
root.append(ul)

books.forEach(el => {
  try {
    if(el.author === undefined){
      throw new Error("No author");
    }else if(el.name === undefined){
      throw new Error("No name")
    }else if (el.price === undefined){
      throw new Error("No price")       
    }else{
        const price = document.createElement("li"),
        name = document.createElement("li"),
        author = document.createElement("li");

        price.textContent = `Price:${el.price}`;
        name.textContent = `Name:${el.name}`;
        author.textContent = `Author:${el.author}`;

        ul.append(name,author,price);
    }
  } catch (error) {
    console.log(error);
  }
});
  